<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Validator;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return $this->showAll($users);
    }    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ])->validate();

        $user = new User();

        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = $request->password;

        if (!$user->save()) {
            return [
                'status' => 203,
                'message' => "Ups, Ocurrio un error durante la creación del usuario",                
            ];
        } else {
            return [
                'status' => 201,
                'message' => "Usuario Creado Exitosamente",
                'data' => $user
            ];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    public function show(User $user)
    {
        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {        

        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->password = $request->password;

        if (!$user->save()) {
            return [
                'status' => 203,
                'message' => "Ups, Ocurrio un error durante la actualización del usuario",
            ];
        } else {
            return [
                'status' => 200,
                'message' => "Usuario actualizado Exitosamente",
                'data' => $user
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->id === Auth::id()) {

            return [
                'error'   => 'No te puedes eliminar a ti mismo',
            ];            

        } else {            
            $user->delete();

            return [
                'status'  => 200,
                'message'   => 'Se ha eliminado el usuario exitosamente',
            ];
            
        }           

    }
}
