<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Consts
    |--------------------------------------------------------------------------    
    */

    const AVAILABLE_PRODUCT = true;    
    const NOT_AVAILABLE_PRODUCT = false;

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------    
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'quantity',
        'status',
        'image',
        'seller_id'
    ];

    /*
    |--------------------------------------------------------------------------
    | Methods
    |--------------------------------------------------------------------------    
    */

    /**
     * Determina si un producto esta disponible
     * 
     * @return boolean
     */
    public function isAvailable(): bool
    {
        return $this->status == Product::AVAILABLE_PRODUCT;
    }

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------    
    */

    /**
     * Un producto pertenece a un vendedor
     * 
     * @return Seller
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    /**
     * Un producto tiene muchas transacciones
     * 
     * @return Transaction
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * Un producto pertenece a muchas categorias
     * 
     * @return Category
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
