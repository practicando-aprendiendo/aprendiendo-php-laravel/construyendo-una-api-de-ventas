<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------    
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationshipts
    |--------------------------------------------------------------------------    
    */

    /**
     * Una categoria pertenece a muchos productos
     * 
     * @return Product
     */
    function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
