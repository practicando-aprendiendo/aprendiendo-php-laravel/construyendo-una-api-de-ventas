<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    /*
    |--------------------------------------------------------------------------
    | Attributes
    |--------------------------------------------------------------------------    
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [        
        'quantity',        
        'buyer_id',
        'product_id',
    ];

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------    
    */

    /**
     * Una transaccion pertenece a un comprador
     * 
     * @return Buyer
     */
    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    /**
     * Una transaccion pertenece a un producto
     * 
     * @return Product
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
